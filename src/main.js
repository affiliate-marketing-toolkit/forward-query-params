const DAY = 24 * 60 * 60 * 1000

const serialize = (obj) => {
  let str = []
  for (let p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(`${encodeURIComponent(p)}=${encodeURIComponent(obj[p])}`)
    }
  return str.join('&')
}

const getQueries = (search) => {
  const query = {}
  const queryString = search || window.location.search
  const pairs = (queryString[0] === '?' ? queryString.substr(1) : queryString).split('&');
  for (let i = 0; i < pairs.length; i++) {
    const pair = pairs[i].split('=')
    if (decodeURIComponent(pair[0])) {
      query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '')
    }
  }
  return query
}

const setCookie = (name, value, days = 9999) => {
  let expires = ''
  if (days) {
    const date = new Date()
    date.setTime(date.getTime() + (days * DAY))
    expires = `; expires=${date.toUTCString()}`
  }
  document.cookie = `${name}=${value || ''};expires=${expires};path=/;samesite=none;secure`
}

const getCookie = (name) => {
  const value = `; ${document.cookie}`
  const parts = value.split(`; ${name}=`)
  if (parts.length === 2) return parts.pop().split(';').shift()
  return null
}

const saveAllQueries = () => {
  const queries = getQueries()
  const queryCookies = JSON.parse(getCookie('sq') || '[]')

  for (let key in queries) {
    if (queries.hasOwnProperty(key)) {
      setCookie(key, queries[key])
      if (queryCookies.indexOf(key) < 0) {
        queryCookies.push(key)
      }
    }
  }

  setCookie('sq', JSON.stringify(queryCookies))
}

const getAllCookies = () => {
  const queryCookies = JSON.parse(getCookie('sq') || '[]')
  const savedQueries = document.cookie.split(';').filter(s => {
    const [ name, value ] = s.split('=').map(c => c.trim())
    return queryCookies.indexOf(name) >= 0
  }).reduce((cookies, cookie) => {
    const [ name, value ] = cookie.split('=').map(c => c.trim())
    cookies[name] = value
    return cookies
    }, {})

  return savedQueries
}

const linkHasMacros = url => url ? url.match(/({[a-zA-Z0-9_.-]+})/g) : null
const replaceEmptyMacros = url => url ? url.replace(/({[a-zA-Z0-9_.-]+})/g, '') : url
const replaceEmptyQueries = search => {
  if (search) {
    let queries = getQueries(search)
    Object.keys(queries).forEach(key => !queries[key] && delete queries[key])
    return serialize(queries)
  }
  return search
}

const startForwardQueryParams = () => {
  saveAllQueries()
  console.log(getAllCookies())

  document.addEventListener('click', e  => {
    // e.preventDefault()
    const { target } = e
    if (target.hasAttribute('href')) {
      let url = decodeURIComponent(target.href)
      const savedQueries = getAllCookies()
      const linkMacros = linkHasMacros(url)

      if (!!linkMacros) {
        for (let key in savedQueries) {
          if (savedQueries.hasOwnProperty(key) && linkMacros.indexOf(`{${key}}`) >= 0) {
            url = url.replace(new RegExp(`{${key}}`, 'g'), savedQueries[key])
          }
        }
        target.href = replaceEmptyMacros(url)
        target.search = replaceEmptyQueries(target.search)
      } else {
        const queries = getQueries(target.search)
        target.search = `?${serialize(Object.assign(queries, savedQueries))}`
      }
    }
  })
}

window.addEventListener('DOMContentLoaded', startForwardQueryParams)
